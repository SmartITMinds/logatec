# Generated by Django 2.1.5 on 2019-08-01 00:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Korisnici', '0048_auto_20190731_2353'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='dobitnici',
            name='winner',
        ),
        migrations.AddField(
            model_name='event',
            name='winner',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
    ]
