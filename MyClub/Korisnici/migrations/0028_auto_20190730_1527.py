# Generated by Django 2.1.5 on 2019-07-30 15:27

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('Korisnici', '0027_auto_20190730_1523'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='id',
            field=models.AutoField(auto_created=True, default=1, primary_key=True, serialize=False, verbose_name='ID'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='event',
            name='selection',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='Korisnici.Selekcije'),
        ),
    ]
