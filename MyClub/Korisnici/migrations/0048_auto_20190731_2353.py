# Generated by Django 2.1.5 on 2019-07-31 23:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Korisnici', '0047_auto_20190731_2315'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dobitnici',
            name='winner',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
    ]
