# Generated by Django 2.1.5 on 2019-08-01 20:43

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Korisnici', '0050_event_dobitnik'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='event',
            name='dobitnik',
        ),
    ]
