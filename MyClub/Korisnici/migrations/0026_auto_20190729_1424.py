# Generated by Django 2.1.5 on 2019-07-29 14:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Korisnici', '0025_auto_20190729_1255'),
    ]

    operations = [
        migrations.AddField(
            model_name='ucesnici',
            name='event_id',
            field=models.FloatField(null=True),
        ),
        migrations.AddField(
            model_name='ucesnici',
            name='role',
            field=models.CharField(max_length=30, null=True),
        ),
    ]
