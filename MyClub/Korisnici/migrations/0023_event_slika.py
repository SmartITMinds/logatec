# Generated by Django 2.1.5 on 2019-07-29 12:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Korisnici', '0022_auto_20190729_1244'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='slika',
            field=models.ImageField(blank=True, max_length=255, null=True, upload_to='slike/%Y/%m'),
        ),
    ]
