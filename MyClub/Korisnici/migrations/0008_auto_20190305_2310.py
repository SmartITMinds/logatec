# Generated by Django 2.1.5 on 2019-03-05 22:10

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Korisnici', '0007_auto_20190305_2310'),
    ]

    operations = [
        migrations.RenameField(
            model_name='korisnik',
            old_name='users',
            new_name='user',
        ),
    ]
