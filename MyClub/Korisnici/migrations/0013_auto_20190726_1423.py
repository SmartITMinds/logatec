# Generated by Django 2.1.5 on 2019-07-26 14:23

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('Korisnici', '0012_auto_20190726_1339'),
    ]

    operations = [
        migrations.AddField(
            model_name='raspored',
            name='id',
            field=models.AutoField(auto_created=True, default=1, primary_key=True, serialize=False, verbose_name='ID'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='raspored',
            name='selekcija',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='Korisnici.Selekcije'),
        ),
    ]
