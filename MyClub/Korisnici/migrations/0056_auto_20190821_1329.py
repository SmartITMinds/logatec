# Generated by Django 2.1.5 on 2019-08-21 13:29

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Korisnici', '0055_auto_20190820_1334'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='schedule',
            options={'verbose_name_plural': 'Schedule'},
        ),
        migrations.AlterModelOptions(
            name='selekcije',
            options={'verbose_name_plural': 'Selections'},
        ),
        migrations.AlterModelOptions(
            name='vesti',
            options={'verbose_name_plural': 'News'},
        ),
        migrations.RenameField(
            model_name='selekcije',
            old_name='naziv',
            new_name='name',
        ),
    ]
