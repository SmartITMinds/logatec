from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver


# Create your models here.

#TABELA SELEKCIJE

class Selekcije(models.Model):

    name = models.CharField(max_length=30,null=True,blank=True)
    main_picture = models.ImageField(blank=True, null=True, upload_to="slike/%Y/%m", max_length=255)
    class Meta:
        verbose_name_plural = "Selections"
    def __str__(self):
        return self.name

# TABELA KORISNICI

class Korisnik (models.Model):
    class Meta:
        verbose_name_plural = "Users additional data"
    user = models.OneToOneField(User, primary_key=True, on_delete=models.CASCADE)
    phone = models.CharField(max_length=20,blank=False)
    selection = models.ForeignKey(
        'Selekcije',
        on_delete=models.CASCADE,
        default=6,
    )
    date_of_birth = models.CharField(max_length=25, blank=True, null=True)
    notification = models.BooleanField(default=True)
    def __str__(self):
        return self.user.username

@receiver(post_save, sender= User)
def create_user_profile(sender, instance, created, **kwargs):
        if created:Korisnik.objects.create(user=instance)

#TABELA VESTI

class Vesti(models.Model):
    picture = models.ImageField(blank=True, null=True, upload_to="slike/%Y/%m", max_length=255)
    date = models.DateField()
    title = models.CharField(max_length=30, null=True)
    text = models.TextField(max_length=1000, null=True)
    hashtag = models.CharField(max_length=20, null=True, blank=True)
    selection = models.ForeignKey(Selekcije, on_delete=models.CASCADE, blank=True, null=True)
    
    class Meta:
        verbose_name_plural = "News"

    def __str__(self):
        return self.title

#TABELA Event

class Event(models.Model):
    name_of_the_event= models.CharField(max_length=30, null=True)
    date = models.DateField()
    sponsor = models.CharField(max_length=30, null=True)
    prize = models.CharField(max_length=30, null=True)
    selection = models.ForeignKey(Selekcije, on_delete=models.CASCADE, null=True)
    picture = models.ImageField(blank=True, null=True, upload_to="slike/%Y/%m", max_length=255)
    winner = models.CharField(max_length=50, null=True, blank=True)
    class Meta:
        verbose_name_plural = "Event"

    def __str__(self):
        return self.name_of_the_event

#TABELA DOBITNICI

class Dobitnici(models.Model):
    date = models.DateField(auto_now_add=True, null=True)
    event = models.ForeignKey(Event, on_delete=models.CASCADE, null=True)
    class Meta:
        verbose_name_plural = "Dobitnici"
    def __str__(self):
        return self.event.name_of_the_event

# TABELA RASPORED

class Schedule(models.Model):
    selection = models.OneToOneField(
        Selekcije,
        on_delete=models.CASCADE,
        primary_key=True,
    )
    Coach = models.CharField(max_length=50,null=True, blank=True)

    Mondey = models.CharField(max_length=30,null=True, blank=True)
    Tuesday = models.CharField(max_length=30,null=True, blank=True)
    Wednesday = models.CharField(max_length=30,null=True, blank=True)
    Thursday = models.CharField(max_length=30,null=True, blank=True)
    Friday = models.CharField(max_length=30,null=True, blank=True)
    Saturday = models.CharField(max_length=30,null=True, blank=True)
    Sunday = models.CharField(max_length=30,null=True, blank=True)

    class Meta:
        verbose_name_plural = "Schedule"
    def __str__(self):
        return self.selekcija.name

#TABELA Desavanja

class Standings(models.Model):
    selection = models.ForeignKey(Selekcije, on_delete=models.CASCADE, null=True)
    league = models.CharField(max_length=70, null=True)
    position = models.FloatField(null=True, blank=True)
    points = models.FloatField(null=True , blank=True)
    played = models.FloatField(null=True , blank=True)
    wins = models.FloatField(null=True , blank=True)
    loses = models.FloatField(null=True , blank=True)
    WL = models.CharField(max_length=70, null=True)
    
    class Meta:
        verbose_name_plural = "Standings"

    def __str__(self):
        return self.selection.name

class Players(models.Model):
    selection = models.ForeignKey(Selekcije, on_delete=models.CASCADE, null=True)
    picture = models.ImageField(blank=True, null=True, upload_to="slike/%Y/%m", max_length=255)
    name_of_player = models.CharField(max_length=70, null=True)
    date_of_birth = models.CharField(max_length=70, null=True)
    height = models.CharField(max_length=70, null=True)
    position = models.CharField(max_length=70, null=True)

    class Meta:
        verbose_name_plural = "Players"

    def __str__(self):
        return self.name_of_player


class Ucesnici(models.Model):
    username = models.CharField(max_length=30, null=True, blank=True, default='Pera')
    longitude = models.FloatField(null=True)
    latitude = models.FloatField(null=True)
    datum = models.DateTimeField(auto_now_add=True, null=True)
    event_id = models.FloatField(null=True)
    role = models.CharField(max_length=30, null=True)

    class Meta:
        verbose_name_plural = "Nagradna igra"

    def __str__(self):
        return self.username

