from django.urls import path, include
from . import views
from rest_framework import routers
from .views import GameClass
router = routers.DefaultRouter()
router.register('korisnici', views.KorisnikView),
router.register('user', views.UserView),
router.register('vesti', views.VestiView),
router.register('raspored', views.ScheduleView),
router.register('dobitnici', views.DobitniciView),
router.register('ucesnici', views.UcesniciView),
router.register('selekcije', views.SelekcijaView),
router.register('event', views.EventView),
router.register('standings', views.StandingsView),
router.register('players', views.PlayersView),
urlpatterns = [
    #path('push/', push),
    #path('push/send_push', send_push),
    path('game/', GameClass.as_view()),
    #path('delete/', DeleteClass.as_view()),
    path('',include(router.urls)),
    ]
#DeleteClass