from django.shortcuts import render, HttpResponse, get_object_or_404
from rest_framework import viewsets
from .models import Korisnik, User, Vesti, Dobitnici, Schedule, Ucesnici, Selekcije, Event, Standings, Players
from .serializers import KorisnikSerializer, UserSerializer, VestiSerializer , ScheduleSerializer, DobitniciSerializer, UcesniciSerializer, SelekcijaSerializer, EventSerializer, StandingsSerializer, PlayersSerializer
#from rest_framework.authentication import SessionAuthentication, BaseAuthentication
from rest_framework.permissions import IsAuthenticated
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters
import http.client
import json
import requests
from django.core import serializers
from django.http import JsonResponse
from django.views.decorators.http import require_GET, require_POST
from django.views.decorators.csrf import csrf_exempt
import random
from django.db import connection
from datetime import datetime
from collections import Counter
from rest_framework import generics
from math import sin, cos, sqrt, atan2, radians


class UserView(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    filter_backends = (DjangoFilterBackend, filters.SearchFilter,)
    filterset_fields = ('id',)
    search_fields = ('username',)

class KorisnikView(viewsets.ModelViewSet):
    queryset = Korisnik.objects.all()
    serializer_class = KorisnikSerializer

class SelekcijaView(viewsets.ModelViewSet):
    queryset = Selekcije.objects.all().order_by('-id')
    serializer_class = SelekcijaSerializer

class VestiView(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    queryset = Vesti.objects.all().order_by('-date')
    serializer_class = VestiSerializer
    filter_backends = (DjangoFilterBackend, filters.SearchFilter,)
    filterset_fields = ('hashtag','selection',)
    search_fields = ('title','date',)

class DobitniciView(viewsets.ModelViewSet):
    #permission_classes = (IsAuthenticated,)
    queryset = Dobitnici.objects.all().order_by('-date')
    serializer_class = DobitniciSerializer

class EventView(viewsets.ModelViewSet):
    #permission_classes = (IsAuthenticated,)
    queryset = Event.objects.all().order_by('date')
    serializer_class = EventSerializer

class ScheduleView(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    queryset = Schedule.objects.all().order_by('-selection')
    serializer_class = ScheduleSerializer

class UcesniciView(viewsets.ModelViewSet):
    queryset = Ucesnici.objects.all()
    serializer_class = UcesniciSerializer

class StandingsView(viewsets.ModelViewSet):
    queryset = Standings.objects.all().order_by('-selection')
    serializer_class = StandingsSerializer

class PlayersView(viewsets.ModelViewSet):
    queryset = Players.objects.all().order_by('-selection')
    serializer_class = PlayersSerializer
    filter_backends = (DjangoFilterBackend, filters.SearchFilter,)
    filterset_fields = ('selection',)

class GameClass(generics.ListCreateAPIView):

    def post(self, request, *args, **kwargs):
        my_list=[]
        my_list1=[]
        R = 6373.0
        event=request.data
        x=event['event']
        queryset = Ucesnici.objects.all().filter(event_id=x)
        z=queryset.filter(role="Admin")
        print(z)
        for a in z:
            lat=(a.latitude)
            lon=(a.longitude)
        print(lat, lon)
        users=queryset.filter(role="Fan")
        print(users)
        for u in users:
            latit=(u.latitude)
            longi=(u.longitude)
            print(latit,longi, u.username)    

            lat1 = radians(lat)
            lon1 = radians(lon)
            lat2 = radians(latit)
            lon2 = radians(longi)
            dlon = lon2 - lon1
            dlat = lat2 - lat1

            a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
            c = 2 * atan2(sqrt(a), sqrt(1 - a))

            distance = R * c
            if distance < 1:
                my_list.append(u.username)
            else:
                my_list1.append(u.username)
            print(my_list, "Ljudi koju su blizu")
            print(my_list1, "Ljudi koji su daleko")
            print("Result:", distance)
        data={}
        data['dobitnik'] =  random.choice(my_list)   
        print("Dobitnik je :", data['dobitnik'])
        data['ucestvuju'] = len(my_list)
        data['neucestvuju'] = len(my_list1)
        json_data=json.dumps(data)
        return JsonResponse(json_data, safe=False)

   # def post(self, request, *args, **kwargs):
    #    test=[]
    #    test1=[]
     #   queryset = Ucesnici.objects.all()
     #   my_list=[]
      #  my_list1=[]
     #   my_list2=[]
     #   my_list3=[]
     #   for x in queryset:
     #       longti=x.longitude*10000
     #       longtiTest=int(round(longti,2))
    #        my_list.append(longtiTest)
    #        my_list1.append(x.username)
     #   for z in queryset:
      #      latit=z.latitude*10000
     #       latitudeTest=int(round(latit,2))
     #       my_list2.append(latitudeTest)
     #       my_list3.append(z.username)
     #   for x in my_list:
     #       if x>100000 and x<140000:
     #           test.append(x)
     #   print(test)
    #    for x in my_list2:
     #       if x>400000 and x<450000:
     #           test1.append(x)
     #   print(test)
     #   lista2={}
    #    lista1={}
     #   UcesNaIstojLokaciji1=[]
     #   UcesNaIstojLokaciji2=[]
     #   lista1= zip(my_list1, my_list)
     #   lista1= dict(lista1)
     #   lista2= zip(my_list3, my_list2)
     #   lista2= dict(lista2)
     #   my_list.sort()
    #    my_list2.sort()
     #   print(my_list,"test")
     #   for key,value in my_list:
     #       rezultat = key      
     #   for key,value in my_list2:
    #        rezultat2 = key
    #    for key, value in lista1.items():
     #       if(value==rezultat):
     #           UcesNaIstojLokaciji1.append(key)     
     #   for key,value in lista2.items():
     #       if(value==rezultat2):
     #           UcesNaIstojLokaciji2.append(key)
     #   return JsonResponse(my_list, safe=False)


#class DeleteClass(generics.ListCreateAPIView):
    #def get(self, request, *args, **kwargs):
      #  x =  Ucesnici.objects.all().delete()
      #  a = 0
       # return JsonResponse(a, safe=False)


#@require_GET
#def push(request):
#    return render(request, 'pushnotification.html')

#
#@csrf_exempt
#def send_push(request):
    #print(request.POST)
    #title = request.POST.get('title')
    #notification = request.POST.get('notification')
    #url = 'https://fcm.googleapis.com/fcm/send'
    #headers = {
    #    'Authorization': 'key=AIzaSyCyPxqh-FPmvXjkx1lqrL2qGRtErqpat_E',
    #    'Content-Type': 'application/json',
    #}
    #payload =   {
    #    "to":"/topics/all",
    #        "notification": 
    #            {
    #            "title": title ,
    #            "body":  notification ,
    #            "content_available" : 'true',
    #            "priority" : "high",
    #            }
    #}
    #requests.post(url, headers=headers,data=json.dumps(payload))
    #return render(request, 'pushnotification.html')