from django.contrib import admin
from Korisnici.models import Korisnik, Dobitnici, Schedule, Vesti, Selekcije, Ucesnici, Event, Standings, Players
# Register your models here.
admin.site.register(Korisnik)
#admin.site.register(Dobitnici)
admin.site.register(Schedule)
admin.site.register(Vesti)
admin.site.register(Selekcije)
#admin.site.register(Ucesnici)
admin.site.register(Event)
admin.site.register(Standings)
admin.site.register(Players)