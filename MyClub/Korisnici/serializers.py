from rest_framework import serializers
from .models import Korisnik, User , Vesti , Dobitnici , Schedule, Ucesnici, Selekcije, Event, Standings, Players
from django.contrib.auth.hashers import make_password
from django.contrib.auth.hashers import PBKDF2PasswordHasher
from django.contrib.auth.models import User, Group
from django.db.models.signals import post_save
from django.dispatch import receiver

class UserSerializerCustom (serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username',)

class UserSerializer (serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username','first_name','password','email','pk','groups','id')
        
    def create(self, validated_data,):
        user = User.objects.create(
        email= validated_data['email'],
        username= validated_data['username'],
        password = make_password(validated_data['password']),
        first_name = validated_data['first_name'],
        )
        return user
    
    def update(self, instance, validated_data):
        instance.set_password(validated_data['password'])
        instance.first_name= validated_data['first_name']
        instance.email= validated_data['email']
        instance.save()
        return instance

@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        instance.groups.add(Group.objects.get(name='Fan'))

class SelekcijaSerializer (serializers.ModelSerializer):
    class Meta:
        model = Selekcije
        fields = ('name','id','pk','main_picture')

class KorisnikSerializer (serializers.ModelSerializer):
    class Meta:
        model = Korisnik
        fields = ('pk','phone','url','date_of_birth','user','selection','notification',)

class VestiSerializer (serializers.ModelSerializer):
    picture = serializers.ImageField(max_length=None, use_url=True)
    selection = SelekcijaSerializer(read_only=True)
    class Meta:
        model = Vesti
        fields = ('url', 'pk', 'date', 'title', 'text', 'picture', 'hashtag', 'selection')

class ScheduleSerializer (serializers.ModelSerializer):
    selection = SelekcijaSerializer(read_only=True)
    class Meta:
        model = Schedule
        fields = ('url', 'pk', 'selection', 'Coach','Mondey','Tuesday','Wednesday','Thursday', 'Friday','Saturday','Sunday')
        
class UcesniciSerializer (serializers.ModelSerializer):
    class Meta:
        model = Ucesnici
        fields = ('url', 'pk', 'longitude', 'latitude', 'username', 'datum', 'id','event_id','role')

class EventSerializer (serializers.ModelSerializer):
    selection = SelekcijaSerializer(read_only=True)
    #winner = UserSerializerCustom()
    class Meta:
        model = Event
        fields = ('pk','date', 'name_of_the_event', 'prize', 'selection', 'sponsor','picture','winner')

class DobitniciSerializer (serializers.ModelSerializer):
    class Meta: 
        model = Dobitnici
        fields = ('url', 'pk', 'date', 'event',)

class DobitniciSerializerCustom (serializers.ModelSerializer):
    class Meta: 
        model = Dobitnici
        fields = ('url', 'pk', 'date', 'event','winner')
    
class StandingsSerializer (serializers.ModelSerializer):
    selection = SelekcijaSerializer(read_only=True)
    class Meta:
        model = Standings
        fields = '__all__'

class PlayersSerializer (serializers.ModelSerializer):
    #selection = SelekcijaSerializer(read_only=True)
    class Meta:
        model = Players
        fields = '__all__'