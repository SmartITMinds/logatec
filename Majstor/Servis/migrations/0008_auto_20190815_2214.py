# Generated by Django 2.2.2 on 2019-08-15 20:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Servis', '0007_auto_20190813_2104'),
    ]

    operations = [
        migrations.AddField(
            model_name='saveti',
            name='file2',
            field=models.ImageField(blank=True, max_length=255, null=True, upload_to='slike/%Y/%m'),
        ),
        migrations.AddField(
            model_name='saveti',
            name='file3',
            field=models.ImageField(blank=True, max_length=255, null=True, upload_to='slike/%Y/%m'),
        ),
        migrations.AddField(
            model_name='saveti',
            name='file4',
            field=models.ImageField(blank=True, max_length=255, null=True, upload_to='slike/%Y/%m'),
        ),
        migrations.AddField(
            model_name='saveti',
            name='file5',
            field=models.ImageField(blank=True, max_length=255, null=True, upload_to='slike/%Y/%m'),
        ),
        migrations.AlterField(
            model_name='imam',
            name='kolicina',
            field=models.CharField(blank=True, default='1', max_length=10, null=True),
        ),
        migrations.AlterField(
            model_name='trazim',
            name='mogucepoklapanje',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.DeleteModel(
            name='Notifikacije',
        ),
    ]
