from rest_framework import serializers
from .models import Imam, Trazim, Saveti, User, Korisnik
from django.contrib.auth.hashers import make_password
from django.contrib.auth.hashers import PBKDF2PasswordHasher
from django.contrib.auth.models import User, Group
from django.db.models.signals import post_save
from django.dispatch import receiver

class UserSerializer (serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username','first_name','password','email','pk','groups','id','last_name')
        
    def create(self, validated_data,):
        user = User.objects.create(
        email= validated_data['email'],
        username= validated_data['username'],
        password = make_password(validated_data['password']),
        first_name = validated_data['first_name'],
        last_name = validated_data['last_name'],
        )
        return user
    
    def update(self, instance, validated_data):
        instance.set_password(validated_data['password'])
        instance.first_name= validated_data['first_name']
        instance.last_name= validated_data['last_name']
        instance.email= validated_data['email']
        instance.save()
        return instance

@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        instance.groups.add(Group.objects.get(name='Majstor'))

class KorisnikSerializer (serializers.ModelSerializer):
    class Meta:
        model = Korisnik
        fields = ('pk','telefon','url','datum_rodjenja','user','notification','ime_servisa','adresa','grad','servis_u_garanciji1','servis_u_garanciji2',
        'servis_u_garanciji3','servis_u_garanciji4','servis_u_garanciji5','servis_u_garanciji6','servis_u_garanciji7','servis_u_garanciji8','servis_u_garanciji9',
        'servis_u_garanciji10','firebase_token')

class ImamSerializer (serializers.ModelSerializer):
    class Meta:
        model = Imam
        fields = ("datum","oznaka_ploce","oznaka_ploce2","oznaka_ploce3","model_televizora","proizvodjac_televizora","file","user","id","pk","url","kolicina","ploca_se_nalazi_u","cena")

class TrazimSerializer (serializers.ModelSerializer):
    class Meta:
        model = Trazim
        fields = ("datum","oznaka_ploce","oznaka_ploce2","oznaka_ploce3","model_televizora","proizvodjac_televizora","file","klijent","brojmobkli","user","id","pk","url",
        'mogucepoklapanje')
        
class SavetiSerializer (serializers.ModelSerializer):
    user = UserSerializer()
    class Meta:
        model = Saveti
        fields = "__all__"

class SvePloceSerializer (serializers.ModelSerializer):
    user = UserSerializer()
    class Meta:
        model = Imam
        fields = ("datum","oznaka_ploce","oznaka_ploce2","oznaka_ploce3","model_televizora","proizvodjac_televizora","file","user","id","pk","url","kolicina","ploca_se_nalazi_u","cena")

class UploadSavetSerializer (serializers.ModelSerializer):
    
    class Meta:
        model = Saveti
        fields = "__all__"