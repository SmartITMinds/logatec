from django.contrib import admin
from Servis.models import Trazim, Imam , Saveti, Korisnik

admin.site.register(Trazim)
admin.site.register(Imam)
admin.site.register(Saveti)
admin.site.register(Korisnik)
