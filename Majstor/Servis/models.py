from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
import http.client
import json
import requests
from django.db.models.signals import post_delete
import os

class Korisnik (models.Model):
    class Meta:
        verbose_name_plural = "Korisnik"
    user = models.OneToOneField(User, primary_key=True, on_delete=models.CASCADE)
    telefon = models.CharField(max_length=20,blank=False)
    ime_servisa = models.CharField(max_length=35, blank=True, null=True)
    adresa = models.CharField(max_length=35, blank=True, null=True)
    grad = models.CharField(max_length=35, blank=True, null=True)
    servis_u_garanciji1 = models.CharField(max_length=35, blank=True, null=True)
    servis_u_garanciji2 = models.CharField(max_length=35, blank=True, null=True)
    servis_u_garanciji3 = models.CharField(max_length=35, blank=True, null=True)
    servis_u_garanciji4 = models.CharField(max_length=35, blank=True, null=True)
    servis_u_garanciji5 = models.CharField(max_length=35, blank=True, null=True)
    servis_u_garanciji6 = models.CharField(max_length=35, blank=True, null=True)
    servis_u_garanciji7 = models.CharField(max_length=35, blank=True, null=True)
    servis_u_garanciji8 = models.CharField(max_length=35, blank=True, null=True)
    servis_u_garanciji9 = models.CharField(max_length=35, blank=True, null=True)
    servis_u_garanciji10 = models.CharField(max_length=35, blank=True, null=True)
    datum_rodjenja = models.CharField(max_length=25, blank=True, null=True)
    firebase_token = models.TextField(max_length=200, blank=True, null=True)
    notification = models.BooleanField(default=True)

    def __str__(self):
        return self.user.username

@receiver(post_save, sender = User)
def create_user_profile(sender, instance, created, **kwargs):
        if created:Korisnik.objects.create(user=instance)

        # Ploce koje trebaju korisniku:

class Trazim(models.Model):
    oznaka_ploce = models.CharField(max_length=30, null=True)
    oznaka_ploce2 = models.CharField(max_length=30, null=True, blank=True)
    oznaka_ploce3 = models.CharField(max_length=30, null=True, blank=True)
    model_televizora= models.CharField(max_length=30, null=True,blank=True)
    proizvodjac_televizora = models.CharField(max_length=30, null=True, blank=True)
    file = models.ImageField(blank=True, null=True, upload_to="slike/%Y/%m", max_length=255)
    datum = models.DateField(auto_now_add=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True , blank=True)
    klijent = models.CharField(max_length=40, null=True, blank= True)
    brojmobkli = models.CharField(max_length=30, null=True, blank= True)
    mogucepoklapanje = models.TextField(null=True, blank=True)
    class Meta:
        verbose_name_plural = "Trazim"

    def __str__(self):
        return self.oznaka_ploce
@receiver(models.signals.post_delete, sender=Trazim)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    if instance.file:
        if os.path.isfile(instance.file.path):
            os.remove(instance.file.path)

@receiver(post_save, sender= Trazim)
def search(sender, instance, created, **kwargs):
    if created:
        lista_imam=[]
        lista_imam2=[]
        lista_imam2.append("trazimid")
        lista_imam.append(instance.id)
        konacna_lista=[]
        rez = Imam.objects.filter(oznaka_ploce=instance.oznaka_ploce)
        korisnikFire= Korisnik.objects.all().filter(user=instance.user)
        for x in korisnikFire:
            TOKEN = x.firebase_token
        for x in rez:
            if(x.oznaka_ploce==instance.oznaka_ploce):
                g = Korisnik.objects.all().filter(user_id=x.user.id)
                m = User.objects.all().filter(username=x.user)
                for l in m:
                    lista_imam2.append("ime")
                    lista_imam.append(l.first_name)
                for a in g:
                    lista_imam2.append("userid")
                    lista_imam.append(x.user_id)
                    lista_imam2.append("modul")
                    lista_imam.append(x.oznaka_ploce)
                    lista_imam2.append("idploce")
                    lista_imam.append(x.id)
                    priprema1=zip(lista_imam2, lista_imam)
                    priprema2=dict (priprema1)    
                    if priprema2 not in konacna_lista:
                        konacna_lista.append(priprema2)
        print("1",konacna_lista)                
        aga = Imam.objects.filter(oznaka_ploce2=instance.oznaka_ploce)
        korisnikFire= Korisnik.objects.all().filter(user=instance.user)
        for x in korisnikFire:
            TOKEN = x.firebase_token
        for x in aga:
            if(x.oznaka_ploce2==instance.oznaka_ploce):
                g = Korisnik.objects.all().filter(user_id=x.user.id)
                m = User.objects.all().filter(username=x.user)
                for l in m:
                    lista_imam2.append("ime")
                    lista_imam.append(l.first_name)
                for a in g:
                    lista_imam2.append("userid")
                    lista_imam.append(x.user_id)
                    lista_imam2.append("modul")
                    lista_imam.append(x.oznaka_ploce2)
                    lista_imam2.append("idploce")
                    lista_imam.append(x.id)
                    priprema1=zip(lista_imam2, lista_imam)
                    priprema2=dict (priprema1)    
                    if priprema2 not in konacna_lista:
                        konacna_lista.append(priprema2)
        print("2",konacna_lista)
        caga = Imam.objects.filter(oznaka_ploce3=instance.oznaka_ploce)
        korisnikFire= Korisnik.objects.all().filter(user=instance.user)
        for x in korisnikFire:
            TOKEN = x.firebase_token
        for x in caga:
            if(x.oznaka_ploce3==instance.oznaka_ploce):
                g = Korisnik.objects.all().filter(user_id=x.user.id)
                m = User.objects.all().filter(username=x.user)
                for l in m:
                    lista_imam2.append("ime")
                    lista_imam.append(l.first_name)
                for a in g:
                    lista_imam2.append("userid")
                    lista_imam.append(x.user_id)
                    lista_imam2.append("modul")
                    lista_imam.append(x.oznaka_ploce3)
                    lista_imam2.append("idploce")
                    lista_imam.append(x.id)
                    priprema1=zip(lista_imam2, lista_imam)
                    priprema2=dict (priprema1)    
                    if priprema2 not in konacna_lista:
                        konacna_lista.append(priprema2)
        print("3",konacna_lista)
        #drugi deo
        if(instance.oznaka_ploce2!=""):   
            rez2 = Imam.objects.filter(oznaka_ploce=instance.oznaka_ploce2 and instance.oznaka_ploce2!="")
            korisnikFire= Korisnik.objects.all().filter(user=instance.user)
            for x in korisnikFire:
                TOKEN = x.firebase_token
            for x in rez2:
                if(x.oznaka_ploce==instance.oznaka_ploce2):
                    g = Korisnik.objects.all().filter(user_id=x.user.id)
                    m = User.objects.all().filter(username=x.user)
                    for l in m:
                        lista_imam2.append("ime")
                        lista_imam.append(l.first_name)
                    for a in g:
                        lista_imam2.append("userid")
                        lista_imam.append(x.user_id)
                        lista_imam2.append("modul")
                        lista_imam.append(x.oznaka_ploce)
                        lista_imam2.append("idploce")
                        lista_imam.append(x.id)
                        priprema1=zip(lista_imam2, lista_imam)
                        priprema2=dict (priprema1)    
                        if priprema2 not in konacna_lista:
                            konacna_lista.append(priprema2)
        print("4",konacna_lista)
        if(instance.oznaka_ploce2!=""):   
            agaa = Imam.objects.filter(oznaka_ploce2=instance.oznaka_ploce2  and instance.oznaka_ploce2!="")
            korisnikFire= Korisnik.objects.all().filter(user=instance.user)
            for x in korisnikFire:
                TOKEN = x.firebase_token
            for x in agaa:
                if(x.oznaka_ploce2==instance.oznaka_ploce2):
                    g = Korisnik.objects.all().filter(user_id=x.user.id)
                    m = User.objects.all().filter(username=x.user)
                    for l in m:
                        lista_imam2.append("ime")
                        lista_imam.append(l.first_name)
                    for a in g:
                        lista_imam2.append("userid")
                        lista_imam.append(x.user_id)
                        lista_imam2.append("modul")
                        lista_imam.append(x.oznaka_ploce2)
                        lista_imam2.append("idploce")
                        lista_imam.append(x.id)
                        priprema1=zip(lista_imam2, lista_imam)
                        priprema2=dict (priprema1)    
                        if priprema2 not in konacna_lista:
                            konacna_lista.append(priprema2)
        print("5",konacna_lista)
        if(instance.oznaka_ploce2!=""):   
            caga1 = Imam.objects.filter(oznaka_ploce3=instance.oznaka_ploce2  and instance.oznaka_ploce2!="")
            korisnikFire= Korisnik.objects.all().filter(user=instance.user)
            for x in korisnikFire:
                TOKEN = x.firebase_token
            for x in caga1:
                if(x.oznaka_ploce3==instance.oznaka_ploce2):
                    g = Korisnik.objects.all().filter(user_id=x.user.id)
                    m = User.objects.all().filter(username=x.user)
                    for l in m:
                        lista_imam2.append("ime")
                        lista_imam.append(l.first_name)
                    for a in g:
                        lista_imam2.append("userid")
                        lista_imam.append(x.user_id)
                        lista_imam2.append("modul")
                        lista_imam.append(x.oznaka_ploce3)
                        lista_imam2.append("idploce")
                        lista_imam.append(x.id)
                        priprema1=zip(lista_imam2, lista_imam)
                        priprema2=dict (priprema1)    
                        if priprema2 not in konacna_lista:
                            konacna_lista.append(priprema2)
                #treci deo
        print("6",konacna_lista)
        if(instance.oznaka_ploce3!=""):
            rez3 = Imam.objects.filter(oznaka_ploce=instance.oznaka_ploce3 and instance.oznaka_ploce3!="")
            korisnikFire= Korisnik.objects.all().filter(user=instance.user)
            for x in korisnikFire:
                TOKEN = x.firebase_token
            for x in rez3:
                if(x.oznaka_ploce==instance.oznaka_ploce3):
                    g = Korisnik.objects.all().filter(user_id=x.user.id)
                    m = User.objects.all().filter(username=x.user)
                    for l in m:
                        lista_imam2.append("ime")
                        lista_imam.append(l.first_name)
                    for a in g:
                        lista_imam2.append("userid")
                        lista_imam.append(x.user_id)
                        lista_imam2.append("modul")
                        lista_imam.append(x.oznaka_ploce)
                        lista_imam2.append("idploce")
                        lista_imam.append(x.id)
                        priprema1=zip(lista_imam2, lista_imam)
                        priprema2=dict (priprema1)    
                        if priprema2 not in konacna_lista:
                            konacna_lista.append(priprema2)
        print("7",konacna_lista)
        if(instance.oznaka_ploce3!=""):   
            aga3 = Imam.objects.filter(oznaka_ploce2=instance.oznaka_ploce3 and instance.oznaka_ploce3!="")
            korisnikFire= Korisnik.objects.all().filter(user=instance.user)
            for x in korisnikFire:
                TOKEN = x.firebase_token
            for x in aga3:
                if(x.oznaka_ploce2==instance.oznaka_ploce3):
                    g = Korisnik.objects.all().filter(user_id=x.user.id)
                    m = User.objects.all().filter(username=x.user)
                    for l in m:
                        lista_imam2.append("ime")
                        lista_imam.append(l.first_name)
                    for a in g:
                        lista_imam2.append("userid")
                        lista_imam.append(x.user_id)
                        lista_imam2.append("modul")
                        lista_imam.append(x.oznaka_ploce2)
                        lista_imam2.append("idploce")
                        lista_imam.append(x.id)
                        priprema1=zip(lista_imam2, lista_imam)
                        priprema2=dict (priprema1)    
                        if priprema2 not in konacna_lista:
                            konacna_lista.append(priprema2)
        print("8",konacna_lista)
        if(instance.oznaka_ploce3!=""):                    
            caga4 = Imam.objects.filter(oznaka_ploce3=instance.oznaka_ploce3)
            korisnikFire= Korisnik.objects.all().filter(user=instance.user)
            for x in korisnikFire:
                TOKEN = x.firebase_token
            for x in caga4:
                if(x.oznaka_ploce3==instance.oznaka_ploce3):
                    g = Korisnik.objects.all().filter(user_id=x.user.id)
                    m = User.objects.all().filter(username=x.user)
                    for l in m:
                        lista_imam2.append("ime")
                        lista_imam.append(l.first_name)
                    for a in g:
                        lista_imam2.append("userid")
                        lista_imam.append(x.user_id)
                        lista_imam2.append("modul")
                        lista_imam.append(x.oznaka_ploce3)
                        lista_imam2.append("idploce")
                        lista_imam.append(x.id)
                        priprema1=zip(lista_imam2, lista_imam)
                        priprema2=dict (priprema1)
                        if priprema2 not in konacna_lista:
                            konacna_lista.append(priprema2)
        print(konacna_lista)
        result = json.dumps(konacna_lista)
        url = 'https://fcm.googleapis.com/fcm/send'
        headers = {
                'Authorization': 'key=AIzaSyCLAu8Pq_tyOjmfiqAwHHnW8Iv0b6K5duc',
                'Content-Type': 'application/json',
                }
        payload =   {
                "to": TOKEN,
                "notification": 
                    {
                    "title": 'Obavestenje' ,
                    "body":  "Moguce poklapanje modula" ,
                    "content_available" : 'true',
                    "priority" : "high",
                    },
                    "data":{
                    "podaci": result,
                    "sender":"trazim"
        }
                }
        print(konacna_lista)
        if(konacna_lista!=[]):
            return requests.post(url, headers=headers,data=json.dumps(payload))
# Ploce koje imam na stanju.

class Imam(models.Model):
    
    oznaka_ploce = models.CharField(max_length=30, null=True, blank=False)
    oznaka_ploce2 = models.CharField(max_length=30, null=True, blank=True)
    oznaka_ploce3 = models.CharField(max_length=30, null=True, blank=True)
    kolicina = models.CharField(max_length=10, null=True, blank=True, default="1")
    model_televizora= models.CharField(max_length=30, null=True, blank=True)
    proizvodjac_televizora = models.CharField(max_length=30, null=True, blank=True)
    file = models.ImageField(blank=True, null=True, upload_to="slike/%Y/%m", max_length=255)
    ploca_se_nalazi_u = models.CharField(max_length=50, null=True, blank=True)
    datum = models.DateField(auto_now_add=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True , blank=True)
    cena = models.CharField(max_length=20, null=True, blank=True)
    
    class Meta:
        verbose_name_plural = "Imam"

    def __str__(self):
        return self.oznaka_ploce

@receiver(models.signals.post_delete, sender=Imam)
def auto_delete_file(sender, instance, **kwargs):
    if instance.file:
        if os.path.isfile(instance.file.path):
            os.remove(instance.file.path)

@receiver(post_save, sender= Imam)
def pretraga(sender, instance, created, **kwargs):
    if created:
        lista_tokena=[]
        lista_imam=[]
        lista_imam2=[]
        lista_imam2.append("imammid")
        lista_imam.append(instance.id)
        konacna_lista=[]
        rez = Trazim.objects.filter(oznaka_ploce=instance.oznaka_ploce)
        for a in rez:
            l=a.user
            korisnikFire= Korisnik.objects.all().filter(user=a.user)
            for x in korisnikFire:
                if x.firebase_token not in lista_tokena:
                    lista_tokena.append(x.firebase_token)
        for x in rez:
            if(x.oznaka_ploce==instance.oznaka_ploce):
                m = User.objects.all().filter(username=x.user)
                for l in m:
                    lista_imam2.append("ime")
                    lista_imam.append(l.first_name)
                g = Korisnik.objects.all().filter(user_id=x.user.id)
                for a in g:
                    lista_imam2.append("userid")
                    lista_imam.append(x.user_id)
                    lista_imam2.append("modul")
                    lista_imam.append(x.oznaka_ploce)
                    lista_imam2.append("idploce")
                    lista_imam.append(x.id)
                    priprema1=zip(lista_imam2, lista_imam)
                    priprema2=dict (priprema1)    
                    if priprema2 not in konacna_lista:
                        konacna_lista.append(priprema2)
        print("1",konacna_lista)
        rez = Trazim.objects.filter(oznaka_ploce2=instance.oznaka_ploce)
        for a in rez:
            l=a.user
            korisnikFire= Korisnik.objects.all().filter(user=a.user)
            for x in korisnikFire:
                if x.firebase_token not in lista_tokena:
                    lista_tokena.append(x.firebase_token)
        for x in rez:
            if(x.oznaka_ploce2==instance.oznaka_ploce):
                m = User.objects.all().filter(username=x.user)
                for l in m:
                    lista_imam2.append("ime")
                    lista_imam.append(l.first_name)
                g = Korisnik.objects.all().filter(user_id=x.user.id)
                for a in g:
                    lista_imam2.append("userid")
                    lista_imam.append(x.user_id)
                    lista_imam2.append("modul")
                    lista_imam.append(x.oznaka_ploce2)
                    lista_imam2.append("idploce")
                    lista_imam.append(x.id)
                    priprema1=zip(lista_imam2, lista_imam)
                    priprema2=dict (priprema1)    
                    if priprema2 not in konacna_lista:
                        konacna_lista.append(priprema2)
        print("2",konacna_lista)
        rez = Trazim.objects.filter(oznaka_ploce3=instance.oznaka_ploce)
        for a in rez:
            l=a.user
            korisnikFire= Korisnik.objects.all().filter(user=a.user)
            for x in korisnikFire:
                if x.firebase_token not in lista_tokena:
                    lista_tokena.append(x.firebase_token)
        for x in rez:
            if(x.oznaka_ploce3==instance.oznaka_ploce):
                m = User.objects.all().filter(username=x.user)
                for l in m:
                    lista_imam2.append("ime")
                    lista_imam.append(l.first_name)
                g = Korisnik.objects.all().filter(user_id=x.user.id)
                for a in g:
                    lista_imam2.append("userid")
                    lista_imam.append(x.user_id)
                    lista_imam2.append("modul")
                    lista_imam.append(x.oznaka_ploce3)
                    lista_imam2.append("idploce")
                    lista_imam.append(x.id)
                    priprema1=zip(lista_imam2, lista_imam)
                    priprema2=dict (priprema1)    
                    if priprema2 not in konacna_lista:
                        konacna_lista.append(priprema2)
        print("3",konacna_lista)
        #drugi deo
        if(instance.oznaka_ploce2!=""):   
            rez = Trazim.objects.filter(oznaka_ploce=instance.oznaka_ploce2)
            for a in rez:
                l=a.user
                korisnikFire= Korisnik.objects.all().filter(user=a.user)
                for x in korisnikFire:
                    if x.firebase_token not in lista_tokena:
                        lista_tokena.append(x.firebase_token)
            for x in rez:
                if(x.oznaka_ploce==instance.oznaka_ploce2):
                    m = User.objects.all().filter(username=x.user)
                    for l in m:
                        lista_imam2.append("ime")
                        lista_imam.append(l.first_name)
                    g = Korisnik.objects.all().filter(user_id=x.user.id)
                    for a in g:
                        lista_imam2.append("userid")
                        lista_imam.append(x.user_id)
                        lista_imam2.append("modul")
                        lista_imam.append(x.oznaka_ploce)
                        lista_imam2.append("idploce")
                        lista_imam.append(x.id)
                        priprema1=zip(lista_imam2, lista_imam)
                        priprema2=dict (priprema1)    
                        if priprema2 not in konacna_lista:
                            konacna_lista.append(priprema2)
        print("4",konacna_lista)
        if(instance.oznaka_ploce2!=""):   
            rez = Trazim.objects.filter(oznaka_ploce2=instance.oznaka_ploce2)
            for a in rez:
                l=a.user
                korisnikFire= Korisnik.objects.all().filter(user=a.user)
                for x in korisnikFire:
                    if x.firebase_token not in lista_tokena:
                        lista_tokena.append(x.firebase_token)
            for x in rez:
                if(x.oznaka_ploce2==instance.oznaka_ploce2):
                    m = User.objects.all().filter(username=x.user)
                    for l in m:
                        lista_imam2.append("ime")
                        lista_imam.append(l.first_name)
                    g = Korisnik.objects.all().filter(user_id=x.user.id)
                    for a in g:
                        lista_imam2.append("userid")
                        lista_imam.append(x.user_id)
                        lista_imam2.append("modul")
                        lista_imam.append(x.oznaka_ploce2)
                        lista_imam2.append("idploce")
                        lista_imam.append(x.id)
                        priprema1=zip(lista_imam2, lista_imam)
                        priprema2=dict (priprema1)    
                        if priprema2 not in konacna_lista:
                            konacna_lista.append(priprema2)
        print("5",konacna_lista)
        if(instance.oznaka_ploce2!=""):
            rez = Trazim.objects.filter(oznaka_ploce3=instance.oznaka_ploce2)
            for a in rez:
                l=a.user
                korisnikFire= Korisnik.objects.all().filter(user=a.user)
                for x in korisnikFire:
                    if x.firebase_token not in lista_tokena:
                        lista_tokena.append(x.firebase_token)
            for x in rez:
                if(x.oznaka_ploce3==instance.oznaka_ploce):
                    m = User.objects.all().filter(username=x.user)
                    for l in m:
                        lista_imam2.append("ime")
                        lista_imam.append(l.first_name)
                    g = Korisnik.objects.all().filter(user_id=x.user.id)
                    for a in g:
                        lista_imam2.append("userid")
                        lista_imam.append(x.user_id)
                        lista_imam2.append("modul")
                        lista_imam.append(x.oznaka_ploce3)
                        lista_imam2.append("idploce")
                        lista_imam.append(x.id)
                        priprema1=zip(lista_imam2, lista_imam)
                        priprema2=dict (priprema1)    
                        if priprema2 not in konacna_lista:
                            konacna_lista.append(priprema2)
        #treci deo
        print("6",konacna_lista)
        if(instance.oznaka_ploce3!=""):
            rez = Trazim.objects.filter(oznaka_ploce=instance.oznaka_ploce3)
            for a in rez:
                l=a.user
                korisnikFire= Korisnik.objects.all().filter(user=a.user)
                for x in korisnikFire:
                    if x.firebase_token not in lista_tokena:
                        lista_tokena.append(x.firebase_token)
            for x in rez:
                if(x.oznaka_ploce==instance.oznaka_ploce):
                    m = User.objects.all().filter(username=x.user)
                    for l in m:
                        lista_imam2.append("ime")
                        lista_imam.append(l.first_name)
                    g = Korisnik.objects.all().filter(user_id=x.user.id)
                    for a in g:
                        lista_imam2.append("userid")
                        lista_imam.append(x.user_id)
                        lista_imam2.append("modul")
                        lista_imam.append(x.oznaka_ploce)
                        lista_imam2.append("idploce")
                        lista_imam.append(x.id)
                        priprema1=zip(lista_imam2, lista_imam)
                        priprema2=dict (priprema1)    
                        if priprema2 not in konacna_lista:
                            konacna_lista.append(priprema2)
        print("7",konacna_lista)
        if(instance.oznaka_ploce3!=""):
            rez = Trazim.objects.filter(oznaka_ploce2=instance.oznaka_ploce3)
            for a in rez:
                l=a.user
                korisnikFire= Korisnik.objects.all().filter(user=a.user)
                for x in korisnikFire:
                    if x.firebase_token not in lista_tokena:
                        lista_tokena.append(x.firebase_token)
            for x in rez:
                if(x.oznaka_ploce2==instance.oznaka_ploce3):
                    m = User.objects.all().filter(username=x.user)
                    for l in m:
                        lista_imam2.append("ime")
                        lista_imam.append(l.first_name)
                    g = Korisnik.objects.all().filter(user_id=x.user.id)
                    for a in g:
                        lista_imam2.append("userid")
                        lista_imam.append(x.user_id)
                        lista_imam2.append("modul")
                        lista_imam.append(x.oznaka_ploce2)
                        lista_imam2.append("idploce")
                        lista_imam.append(x.id)
                        priprema1=zip(lista_imam2, lista_imam)
                        priprema2=dict (priprema1)    
                        if priprema2 not in konacna_lista:
                            konacna_lista.append(priprema2)
        print("8",konacna_lista)
        if(instance.oznaka_ploce3!=""):
            rez = Trazim.objects.filter(oznaka_ploce3=instance.oznaka_ploce3)
            for a in rez:
                l=a.user
                korisnikFire= Korisnik.objects.all().filter(user=a.user)
                for x in korisnikFire:
                    if x.firebase_token not in lista_tokena:
                        lista_tokena.append(x.firebase_token)
            for x in rez:
                if(x.oznaka_ploce3==instance.oznaka_ploce):
                    m = User.objects.all().filter(username=x.user)
                    for l in m:
                        lista_imam2.append("ime")
                        lista_imam.append(l.first_name)
                    g = Korisnik.objects.all().filter(user_id=x.user.id)
                    for a in g:
                        lista_imam2.append("userid")
                        lista_imam.append(x.user_id)
                        lista_imam2.append("modul")
                        lista_imam.append(x.oznaka_ploce3)
                        lista_imam2.append("idploce")
                        lista_imam.append(x.id)
                        priprema1=zip(lista_imam2, lista_imam)
                        priprema2=dict (priprema1)    
                        if priprema2 not in konacna_lista:
                            konacna_lista.append(priprema2)
        print("9",konacna_lista)
        print("TOKENI :", lista_tokena)
        result = json.dumps(konacna_lista)
        url = 'https://fcm.googleapis.com/fcm/send'    
        for token in lista_tokena:
            print(token)
            headers = {
                'Authorization': 'key=AIzaSyCLAu8Pq_tyOjmfiqAwHHnW8Iv0b6K5duc',
                'Content-Type': 'application/json',
                    }
            payload =   {
                "to": token ,
                "notification": 
                    {
                    "title": 'Obavestenje!' ,
                    "body":  "Moguce poklapanje modula" ,
                    "content_available" : 'true',
                    "priority" : "high",
                    },
                    "data":{
                    "podaci": result,
                    "sender":"imam"
                    }
                }
            x= requests.post(url, headers=headers,data=json.dumps(payload))

# Saveti za popravku televizora
class Saveti(models.Model):
    
    opis_kvara = models.CharField(max_length=100, null=True, blank=False)
    model_televizora= models.CharField(max_length=30, null=True)
    proizvodjac_televizora = models.CharField(max_length=30, null=True,blank=True)
    savet = models.TextField(null=True, blank=True)
    file = models.ImageField(blank=True, null=True, upload_to="slike/%Y/%m", max_length=255)
    file2 = models.ImageField(blank=True, null=True, upload_to="slike/%Y/%m", max_length=255)
    file3 = models.ImageField(blank=True, null=True, upload_to="slike/%Y/%m", max_length=255)
    file4 = models.ImageField(blank=True, null=True, upload_to="slike/%Y/%m", max_length=255)
    file5 = models.ImageField(blank=True, null=True, upload_to="slike/%Y/%m", max_length=255)
    datum = models.DateField(auto_now_add=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True , blank=True)
    
    class Meta:
        verbose_name_plural = "Saveti"

    def __str__(self):
        return self.opis_kvara

@receiver(models.signals.post_delete, sender=Saveti)
def auto_delete_file_on(sender, instance, **kwargs):
    if instance.file:
        if os.path.isfile(instance.file.path):
            os.remove(instance.file.path)
    if instance.file2:
        if os.path.isfile(instance.file2.path):
            os.remove(instance.file2.path)
    if instance.file3:
        if os.path.isfile(instance.file3.path):
            os.remove(instance.file3.path)
    if instance.file4:
        if os.path.isfile(instance.file4.path):
            os.remove(instance.file4.path)
    if instance.file5:
        if os.path.isfile(instance.file5.path):
            os.remove(instance.file5.path)