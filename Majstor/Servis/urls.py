from django.urls import path, include
from . import views
from rest_framework import routers

router = routers.DefaultRouter()
router.register('imam', views.ImamView),
router.register('trazim', views.TrazimView),
router.register('saveti', views.SavetiView),
router.register('users', views.UserView),
router.register('korisnik', views.KorisnikView),
router.register('sve',views.SvePloceView),
router.register('savetupload',views.SavetiUploadView),
urlpatterns = [
    path('api/',include(router.urls))
] 