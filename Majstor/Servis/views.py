from django.shortcuts import render
from .models import Imam, Trazim, Saveti, User, Korisnik
from rest_framework import viewsets
from .serializers import ImamSerializer, TrazimSerializer, SavetiSerializer, UserSerializer, KorisnikSerializer, SvePloceSerializer, UploadSavetSerializer
from rest_framework.parsers import FileUploadParser
from rest_framework import status, filters
from rest_framework.response import Response
from django_filters.rest_framework import DjangoFilterBackend

class UserView(viewsets.ModelViewSet):
    queryset = User.objects.all().order_by('first_name')
    serializer_class = UserSerializer
    filter_backends = (DjangoFilterBackend, filters.SearchFilter,)
    filterset_fields = ('id',)
    search_fields = ('username',)

class KorisnikView(viewsets.ModelViewSet):
    queryset = Korisnik.objects.all().order_by('user')
    serializer_class = KorisnikSerializer
    filter_backends = [filters.SearchFilter,]
    search_fields = ['servis_u_garanciji1', 'servis_u_garanciji2', 'servis_u_garanciji3', 'servis_u_garanciji4', 'servis_u_garanciji5',
    'servis_u_garanciji6','servis_u_garanciji7','servis_u_garanciji8','servis_u_garanciji9','servis_u_garanciji10','ime_servisa',]
    
class TrazimView(viewsets.ModelViewSet):
    queryset = Trazim.objects.all().order_by('-datum')
    serializer_class = TrazimSerializer
    filter_backends = [filters.SearchFilter, DjangoFilterBackend]
    search_fields = ['oznaka_ploce', 'oznaka_ploce2', 'oznaka_ploce3', 'model_televizora', 'proizvodjac_televizora']
    filterset_fields = ['user',]
    parser_class = (FileUploadParser,)
     
    def post(self, request, *args, **kwargs):

        trazim_serializer = TrazimSerializer(data=request.data)

        if trazim_serializer.is_valid():
          trazim_serializer.save()
          return Response(trazim_serializer.data, status=status.HTTP_201_CREATED)
        else:
          return Response(trazim_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class ImamView(viewsets.ModelViewSet):
    queryset = Imam.objects.all().order_by('-datum')
    serializer_class = ImamSerializer
    filter_backends = [filters.SearchFilter, DjangoFilterBackend]
    search_fields = ['oznaka_ploce', 'oznaka_ploce2', 'oznaka_ploce3', 'model_televizora', 'proizvodjac_televizora']
    filterset_fields = ['user',]
    parser_class = (FileUploadParser,)
    
class SavetiView(viewsets.ModelViewSet):
    queryset = Saveti.objects.all().order_by('-datum')
    serializer_class = SavetiSerializer
    filter_backends = [filters.SearchFilter, DjangoFilterBackend]
    search_fields = ['model_televizora', 'proizvodjac_televizora', 'opis_kvara']

class SvePloceView(viewsets.ModelViewSet):
    queryset = Imam.objects.all().order_by('-datum')
    serializer_class = SvePloceSerializer
    filter_backends = [filters.SearchFilter, DjangoFilterBackend]
    search_fields = ['oznaka_ploce', 'oznaka_ploce2', 'oznaka_ploce3', 'model_televizora', 'proizvodjac_televizora']
    filterset_fields = ['user',]

class SavetiUploadView(viewsets.ModelViewSet):
    queryset = Saveti.objects.all().order_by('-datum')
    serializer_class = UploadSavetSerializer